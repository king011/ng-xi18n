package cmdupdate

import (
	"fmt"
	"gitlab.com/king011/ng-xi18n/xi18n"
	"io/ioutil"
	"log"
	"os"
)

// Update .
func (c *Context) Update() (e error) {
	_, e = os.Stat(c.Dist)
	if e != nil {
		if os.IsNotExist(e) {
			e = c.copyFile()
		}
		log.Printf("copy %s to %s\n", c.Src, c.Dist)
		return
	}

	//dist
	var b []byte
	b, e = ioutil.ReadFile(c.Dist)
	if e != nil {
		return
	}
	var merge *xi18n.Context
	merge, e = xi18n.NewContext(b)
	if e != nil {
		return
	}
	if c.Locale == "" {
		c.Locale = merge.XML.File.Language
	} else {
		if merge.XML.File.Language != c.Locale {
			e = fmt.Errorf("locale not mathc %s != %s", merge.XML.File.Language, c.Locale)
			return
		}
	}
	log.Printf("find keys %v\n", len(merge.Keys))

	//src
	b, e = ioutil.ReadFile(c.Src)
	if e != nil {
		return
	}
	var src *xi18n.Context
	src, e = xi18n.NewContext(b)
	if e != nil {
		return
	}

	merges := 0
	for i := 0; i < len(src.Items); i++ {
		id := src.Items[i].ID
		if find, ok := merge.Keys[id]; ok {
			log.Printf("%v = %s\n", id, find.Val)
			merges++
			src.Items[i].UpdateTag(find.Val)
		}
	}

	// write file
	var f *os.File
	f, e = os.Create(c.Dist)
	if e != nil {
		return
	}
	e = src.Marshal(f, c.Locale)
	if e == nil {
		for i := 0; i < len(src.Items); i++ {
			id := src.Items[i].ID
			if _, ok := merge.Keys[id]; !ok {
				log.Printf("item %v\n", id)
			}
		}

		log.Printf("merge %s to %s\n", c.Src, c.Dist)
		log.Printf("merge = %v items = %v\n", merges, len(src.Items)-merges)
	}
	f.Close()
	return
}
func (c *Context) copyFile() (e error) {
	var b []byte
	b, e = ioutil.ReadFile(c.Src)
	if e != nil {
		return
	}
	var dist *xi18n.Context
	dist, e = xi18n.NewContext(b)
	if e != nil {
		return
	}

	var f *os.File
	f, e = os.Create(c.Dist)
	if e != nil {
		return
	}
	e = dist.Marshal(f, c.Locale)
	f.Close()
	return
}
